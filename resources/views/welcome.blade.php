<!doctype html>
<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Rental</title>
    <link rel="stylesheet" href="/asset/vendors/ti-icons/css/themify-icons.css">
    <link rel="stylesheet" href="/asset/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="/asset/css/vertical-layout-light/style.css">
    <link rel="stylesheet" href="/asset/vendors/jquery-toast-plugin/jquery.toast.min.css">
    <link href="/css/toggle.css" rel="stylesheet">
    <link href="/css/mystyle.css" rel="stylesheet">
    <link rel="stylesheet" href="/asset/vendors/font-awesome/css/font-awesome.min.css" />
    <link rel="icon" type="image/png" href="/landing/assets/img/favicon.png">
{{--Landing resourec--}}
    {{--<link rel="stylesheet" href="/landing/assets/css/bootstrap.min.css">--}}

    <link rel="stylesheet" href="/landing/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/landing/assets/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="/landing/assets/fonts/flaticon.css">

    <link rel="stylesheet" href="/landing/assets/css/boxicons.min.css">

    <link rel="stylesheet" href="/landing/assets/css/animate.min.css">

    <link rel="stylesheet" href="/landing/assets/css/magnific-popup.css">

    <link rel="stylesheet" href="/landing/assets/css/meanmenu.css">

    <link rel="stylesheet" href="/landing/assets/css/nice-select.min.css">

    <link rel="stylesheet" href="/landing/assets/css/style.css">

    <link rel="stylesheet" href="/landing/assets/css/responsive.css">
    {{--Landing ends--}}

</head>
<body>

<div id="app">
    <app></app>
</div>

<script src="{{ mix('js/app.js') }}"></script>

</script><script src="/landing/assets/js/jquery-3.5.1.slim.min.js"></script>

<script src="/asset/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="/asset/vendors/jquery-steps/jquery.steps.min.js"></script>
<script src="/asset/vendors/jquery-validation/jquery.validate.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="/asset/js/off-canvas.js"></script>
<script src="/asset/js/hoverable-collapse.js"></script>
<script src="/asset/js/template.js"></script>
<script src="/asset/js/settings.js"></script>
<script src="/asset/js/todolist.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="/asset/js/wizard.js"></script>
<!-- End custom js for this page-->

{{--<script src="/js/wizard.js"></script>--}}
<script src="/js/buttonloader.js"></script>
<script src="/asset/vendors/jquery-toast-plugin/jquery.toast.min.js"></script>
<!-- Custom js for this page-->
<script src="/asset/js/toastDemo.js"></script>
{{--<script src="/asset/js/desktop-notification.js"></script>--}}
<!-- End custom js for this page-->
<script src="/asset/vendors/chart.js/Chart.min.js"></script>

<script src="/asset/js/chart.js"></script>
<script src="/asset/js/desktop-notification.js"></script>

{{--Landing --}}
{{--<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js">--}}



<script src="/landing/assets/js/popper.min.js"></script>

{{--<script src="/landing/assets/js/bootstrap.min.js"></script>--}}

<script src="/landing/assets/js/owl.carousel.min.js"></script>
<script src="/landing/assets/js/carousel-thumbs.js"></script>

<script src="/landing/assets/js/meanmenu.js"></script>

<script src="/landing/assets/js/jquery.magnific-popup.min.js"></script>

<script src="/landing/assets/js/wow.min.js"></script>

<script src="/landing/assets/js/jquery.nice-select.min.js"></script>

<script src="/landing/assets/js/jquery.ajaxchimp.min.js"></script>

<script src="/landing/assets/js/form-validator.min.js"></script>

<script src="/landing/assets/js/contact-form-script.js"></script>

<script src="/landing/assets/js/custom.js"></script>
{{--Landin edns--}}

<script src="/loader/center-loader.js"></script>
</body>
<!-- END Body -->
</html>
