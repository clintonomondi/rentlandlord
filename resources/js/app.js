import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import Notifications from 'vue-notification';
import VueTelInput from 'vue-tel-input'
import 'vue-tel-input/dist/vue-tel-input.css'




Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(Notifications);
Vue.use(VueTelInput);

import App from './view/App'
import Index from './pages/index'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
 import Building from './building/Building'
 import Houses from './rooms/Houses'
 import AddRooms from './rooms/AddRooms'
 import Tenant from './tenants/Tenants'
 import HouseCategory from './settings/housecategory'
 import BillingBuilding from './billing/building'
 import BillingRooms from './billing/rooms'
 import GenerateInvoice from './billing/generateInvoice'
 import PendingInvoice from './invoice/pending'
 import PaidInvoice from './invoice/paid'
 import DisputedInvoice from './invoice/disputed'
 import Confirm from './pages/Confirm'
 import Addtenants from './tenants/Addtenants'
 import EditRoom from './rooms/EditRooms'
 import Profile from './pages/Profile'



const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/buildings',
            name: 'buildings',
            component: Building
        },
        {
            path: '/tenants',
            name: 'tenants',
            component: Tenant
        },
        {
            path: '/addrooms/:id',
            name: 'addrooms',
            component: AddRooms
        },
        {
            path: '/houses',
            name: 'houses',
            component: Houses
        },

        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: { hideNavigation: true }
        },
        {
            path: '/house/category',
            name: 'housecategory',
            component: HouseCategory
        },
        {
            path: '/billing/buildings',
            name: 'billingbuildings',
            component: BillingBuilding
        },
        {
            path: '/billing/rooms/:id',
            name: 'billingrooms',
            component: BillingRooms
        },
        {
            path: '/billing/invoice/:id',
            name: 'invoicing',
            component: GenerateInvoice
        },
        {
            path: '/invoice/pending',
            name: 'pendinginvoice',
            component: PendingInvoice
        },
        {
            path: '/invoice/paid',
            name: 'paidinvoice',
            component: PaidInvoice
        },
        {
            path: '/invoice/disputed',
            name: 'disputedinvoice',
            component: DisputedInvoice
        },
        {
            path: '/verify',
            name: 'verify',
            component: Confirm,
            meta: { hideNavigation: true }
        },
        {
            path: '/',
            name: 'index',
            component: Index,
            meta: { hideNavigation: true }
        },
        {
            path: '/addtenants/:id',
            name: 'addtenants',
            component: Addtenants,
        },
        {
            path: '/house/edit/:id',
            name: 'edithouse',
            component: EditRoom,
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile,
        },


    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});

